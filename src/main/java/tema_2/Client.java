package tema_2;

public class Client implements Comparable<Client>{
	private int id;
	public int timpSosire;
	public int timpProcesare;
	public int timpAsteptare;
	
	public Client(int timpSosire, int timpProcesare) {
		this.timpSosire=timpSosire;
		this.timpProcesare=timpProcesare;
	}
	
	/*
	 * Se utiliza pentru sortarea clienților după timpul de sosire
	 * 
	 * @Override*/
	public int compareTo(Client nou) {
		return(new Integer(timpSosire).compareTo(new Integer(nou.timpSosire)));
	}
	
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id=id;
	}
	
	public int getTimpAsteptare() {
		return timpAsteptare;
	}
	
	public void setTimpAsteptare(int timpAsteptare) {
		this.timpAsteptare=timpAsteptare;
	}
	/*
	 * 
	 * @Override*/
	public String toString() {
		return timpProcesare + " ";
	}
	
	public int getTimpSosire() {
		return timpSosire;
	}
	
	public int getTimpFinalizare() {
		return timpProcesare+timpAsteptare+timpSosire;
	}
	
	public int getTimpProcesare() {
		return timpProcesare;
	}

	
	
	

}
