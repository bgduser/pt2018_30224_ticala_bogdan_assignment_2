package tema_2;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Casa implements Runnable {
	public String nume;
	private BlockingQueue<Client> clients;
	private AtomicInteger perioadaAsteptare;
	private boolean open,close;
	private SimulationFrame frame;
	
	public Casa(String nume, SimulationFrame frame) {
		this.nume= new String(nume);
		this.frame=frame;
		clients = new LinkedBlockingQueue<Client>();
		perioadaAsteptare= new AtomicInteger();
		open=false;
		close=false;
		
	}
	
	public void setClose() {
		close=true;
	}
	
	public void setOpen(boolean open) {
		this.open=open;
		
	}
	
	public boolean isOpen() {
		return open;
	}
	
	public AtomicInteger getPerioadaAsteptare() {
		return perioadaAsteptare;
	}
	
	
	public String getName() {
		return nume;
	}
	
	public BlockingQueue<Client> getClients(){
		return clients;
	}
	
	private void frameProcesareClient(Client procesareClient) {
		frame.getInformationClient()
		.append(nume + ": Clientul "+ procesareClient.getId() + " a fost servit dupa ce a asteptat "
					+ procesareClient.getTimpAsteptare() +" secunde, si are timpul de finalizare de "
					+ procesareClient.getTimpFinalizare() +" secunde.\n");
	}
	
	private void frameAjungeClient(Client ajungeClient) {
		frame.getInformationClient().append(nume + ": Clientul "
				+ajungeClient.getId()+ " este servit, timpul de procesare este de "
				+ ajungeClient.getTimpProcesare()+" secunde.\n");
		
	}
	
	 
	public void addClient(Client newClient) {
		// Clientul este adaugat la casa doar daca aceasta este deschisa
		if (open) {
			frame.getInformationClient().append(
					nume + ": Clientul " + newClient.getId() + " a ajuns la secunda " + 
			newClient.getTimpSosire() + ".\n");
			try {
				clients.put(newClient);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			perioadaAsteptare.getAndAdd(newClient.getTimpProcesare());
			newClient.setTimpAsteptare(perioadaAsteptare.get());
		}
	}

	
	/*
	 * @Override
	 * Aceasta va funcționa atâta timp cât Casaele nu sunt închise.
	 *  Se va lua primul client din listă și se va opri funcționarea threadului un
	 *   timp egal cu timpul de procesare al clientului. Interfața se actualizează în 
	 * momentul începereii procesării, dar și după, oferind detalii 
	 * despre timpul de procesare, așteptare și terminare**/
	
	public void run() {
		while (!close) {
			Client processingClient;
			if (!clients.isEmpty()) {
				try {
					processingClient = clients.peek();
					
					frameAjungeClient(processingClient);
					Thread.sleep(processingClient.getTimpProcesare() * 1000);
					
					
					if(close) {
						break;
					}
					
					clients.take();
				
					frameProcesareClient(processingClient);
					
					perioadaAsteptare.getAndAdd(-processingClient.getTimpProcesare());
					
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
	

		}
		}
}
