package tema_2;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import javax.swing.JOptionPane;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;

public class SimulationManager implements Runnable {

	private int timeLimit;
	private int maxProcessingTime;
	private int minProcessingTime;
	private int numarCase;
	private int numarClientiPerCasa;
	private int numarClienti;

	private Scheduler scheduler;
	private List<Client> generareClienti;

	private static SimulationFrame frame = new SimulationFrame();

	public SimulationManager() {
		try {
			// Initializare atribute cu cele din TextField-uri
			// Frame
			generareClienti = new ArrayList<Client>();
			timeLimit = frame.getTimpSimulare();
			maxProcessingTime = frame.getMaxTimpProcesare();
			minProcessingTime = frame.getMinTimpProcesare();

		

			numarCase = frame.getNumarCase();
			numarClientiPerCasa = frame.getNumarClientiPerCasa();
			numarClienti = frame.getNumarClienti();

			
			scheduler = new Scheduler(numarCase, frame);
			scheduler.creareStrategie();
			generareClienti.addAll(generateNRandomClienti());
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "Wrong input!", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	private List<Client> generateNRandomClienti() {
		List<Client> clients = new ArrayList<Client>();

		// Generare Clienti random
		for (int i = 0; i < numarClienti; i++) {
			clients.add(new Client(ThreadLocalRandom.current().nextInt(1, timeLimit),
					ThreadLocalRandom.current().nextInt(minProcessingTime, maxProcessingTime + 1)));
		}

		// Sortare clienti dupa timpul de sosire si setare ID
		
		Collections.sort(clients);
		for (int i = 0; i < numarClienti; i++) {
			clients.get(i).setId(i);
		}

		return clients;
	}

	
	public void run() {

		int currentTime = 0;
		

		try {
			while (currentTime < timeLimit) {
				while (!generareClienti.isEmpty() && generareClienti.get(0).getTimpSosire() == currentTime) {
					// Deschide prima casa
					scheduler.getCasa().get(0).setOpen(true);
					// Opening another server if needed
					openCasa();
					
					// Clientii sunt impartiti dupa StategiaTimp
					scheduler.distribuireClienti(generareClienti.get(0));
					generareClienti.remove(0);
				}

				//Casa fara clienti se va inchide
				closeCasaWithoutClient();

		
				frameEvolutieCasa();

				

				// Incrementare timp si update la frame
				currentTime++;
				frame.setContorTimp(currentTime);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			// Cand timpul de simulare s-a scurs, casa se inchide
			scheduler.close();

			
		} catch (NullPointerException e) {
			JOptionPane.showMessageDialog(null, "Date introduse incorect!", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	

	// Arata evolutia casei de marcat
	private void frameEvolutieCasa() {
		frame.getEvolutieClient().setText("");
		for (Casa s : scheduler.getCasa()) {
			if (s.getClients().isEmpty()) {
				frame.getEvolutieClient().append(s.getName() + ": [CLOSED]\n");
			} else {
				frame.getEvolutieClient().append(s.getName() + ": " + s.getClients() + "\n");
			}
		}
	}

	

	// Deschide alta casa daca e necesar
	private void openCasa() {
		int deschise = 0;
		int capacitateMax = 0;

		// Numar cate case sunt deschise si au numarul maxim de clienti
		for (Casa s : scheduler.getCasa()) {
			if (s.isOpen()) {
				deschise++;
			}
			if (s.getClients().size() >= numarClientiPerCasa) {
				capacitateMax++;
			}
		}

		//Daca casa e deschisa si are maxim clienti, atunci deschiem alta clasa
		
		if (deschise == capacitateMax) {
			for (Casa s : scheduler.getCasa()) {
				if (!s.isOpen()) {
					s.setOpen(true);
					break;
				}
			}
		}
	}

	// Casa fara clienti va fi inchisa
	private void closeCasaWithoutClient() {
		for (Casa s : scheduler.getCasa()) {
			if (s.getClients().isEmpty()) {
				s.setOpen(false);
			} else {
				s.setOpen(true);
			}
		}
	}

	private static void startSimulation() {
		
		frame.getStartBtn().addActionListener(new ActionListener() {
		
			public void actionPerformed(ActionEvent ae) {
				frame.getInformationClient().setText("");
				frame.getEvolutieClient().setText("");
				Thread t = new Thread(new SimulationManager());
				t.start();
			}
		});
	}

	public static void main(String[] args) {

		startSimulation();

	}

}
