package tema_2;

import java.util.ArrayList;
import java.util.List;

public class Scheduler {

	private List<Casa> casa;
	private Strategie strategy;
	
	public Scheduler(int maxNoOfCasa, SimulationFrame frame) {
		casa=new ArrayList<Casa>();
		for(int i=0; i<maxNoOfCasa; i++) {
			Casa c=new Casa ("Casa "+i,frame);
			casa.add(c);
			Thread th=new Thread(c);
			th.start();
		}
		
	}
	
	
	public void close() {
		for(Casa c :casa)
			c.setClose();
	}
	
	public void distribuireClienti(Client cl) {
		strategy.addClient(casa, cl);
	}
	
	public List<Casa> getCasa(){
		return casa;
	}
	
	public void creareStrategie() {
		strategy= new StrategieTimp();
	}
}
