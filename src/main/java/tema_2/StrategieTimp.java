package tema_2;

import java.util.List;

public class StrategieTimp implements Strategie{
	
	
	
	public void addClient(List<Casa> casa, Client c) {
		int minTimpAsteptare= casa.get(0).getPerioadaAsteptare().get();
		
		for( Casa t : casa) {
			if(minTimpAsteptare > t.getPerioadaAsteptare().get() && t.isOpen())
				minTimpAsteptare= t.getPerioadaAsteptare().get();
			
		}
		
		for ( Casa t : casa) {
			if(minTimpAsteptare == t.getPerioadaAsteptare().get()) {
				t.addClient(c);
				c.setTimpAsteptare(t.getPerioadaAsteptare().get());
				break;
			}
		}
	}

}
