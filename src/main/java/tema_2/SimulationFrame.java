package tema_2;

import javax.swing.*;

public class SimulationFrame extends JFrame {

	private static final long serialVersionUID = 1L;

	private JPanel panel = new JPanel();
	
	private JLabel timpSimulareLbl;
	private JLabel minTimpProcesareLbl;
	private JLabel maxTimpProcesareLbl;
	private JLabel numarCaseLbl;
	private JLabel numarClientiPerCasaLbl;
	private JLabel numarClientiLbl;
	
	
	private JTextField timpSimulareTf;
	private JTextField minTimpProcesareTf;
	private JTextField maxTimpProcesareTf;
	private JTextField numarCaseTf;
	private JTextField numarClientiPerCasaTf;
	private JTextField numarClientiTf;
	
	
	private JButton startBtn;
	
	private JTextArea EvolutieClient;
	private JTextArea infoClient;
	
	private JLabel timeLimitLbl;
	private JLabel contorTimpLbl;
	
	public SimulationFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(1000, 625);
		
		add(panel);
		panel.setLayout(null);
		
		
		timpSimulareLbl = new JLabel("Timp de simulare:");
		timpSimulareLbl.setBounds(30, 30, 200, 20);
		
		panel.add(timpSimulareLbl);
		
		timpSimulareTf = new JTextField();
		timpSimulareTf.setBounds(220, 30, 200, 20);
		panel.add(timpSimulareTf);
		
		minTimpProcesareLbl = new JLabel("Timp minim de procesare:");
		minTimpProcesareLbl.setBounds(30, 60, 200, 20);
		
		panel.add(minTimpProcesareLbl);
		
		minTimpProcesareTf = new JTextField();
		minTimpProcesareTf.setBounds(220, 60, 200, 20);
		panel.add(minTimpProcesareTf);
		
		maxTimpProcesareLbl = new JLabel("Timp maxim de procesare:");
		maxTimpProcesareLbl.setBounds(30, 90, 200, 20);
		
		panel.add(maxTimpProcesareLbl);
		
		 maxTimpProcesareTf = new JTextField();
		 maxTimpProcesareTf.setBounds(220, 90, 200, 20);
		panel.add (maxTimpProcesareTf);
		
		numarCaseLbl = new JLabel("Numarul de case:");
		numarCaseLbl.setBounds(30, 120, 200, 20);
		
		panel.add(numarCaseLbl);
		numarCaseTf = new JTextField();
		numarCaseTf.setBounds(220, 120, 200, 20);
		panel.add(numarCaseTf);
		
		numarClientiLbl = new JLabel("Numarul de clienti:");
		numarClientiLbl.setBounds(30, 150, 200, 20);
		
		panel.add(numarClientiLbl);
		
		numarClientiTf = new JTextField();
		numarClientiTf.setBounds(220, 150, 200, 20);
		panel.add(numarClientiTf);
		
		numarClientiPerCasaLbl = new JLabel("Numarul de clienti per casa:");
		numarClientiPerCasaLbl.setBounds(30, 180, 200, 20);
		
		panel.add(numarClientiPerCasaLbl);
		
		numarClientiPerCasaTf = new JTextField();
		numarClientiPerCasaTf.setBounds(220, 180, 200, 20);
		panel.add(numarClientiPerCasaTf);
		
		
		
		startBtn = new JButton("START");
		startBtn.setBounds(30, 270, 390, 30);
		panel.add(startBtn);
		
		EvolutieClient = new JTextArea();
		EvolutieClient.setEditable(false);
		JScrollPane queueScroll = new JScrollPane(EvolutieClient, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		queueScroll.setBounds(450, 60, 520, 240);
		panel.add(queueScroll);
		
		infoClient = new JTextArea();
		infoClient.setEditable(false);
		JScrollPane informationScroll = new JScrollPane(infoClient, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		informationScroll.setBounds(30, 330, 940, 240);
		panel.add(informationScroll);
		
		timeLimitLbl = new JLabel("Time:");
		timeLimitLbl.setBounds(450, 30, 60, 30);
	
		panel.add(timeLimitLbl);
		
		contorTimpLbl = new JLabel("0");
		contorTimpLbl.setBounds(487, 30, 100, 30);
		
		panel.add(contorTimpLbl);
		
		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
	}

	public int getMinTimpProcesare() {
		return Integer.parseInt(minTimpProcesareTf.getText());
	}
	
	public int getMaxTimpProcesare() {
		return Integer.parseInt(maxTimpProcesareTf.getText());
	}
	
	public int getTimpSimulare() {
		return Integer.parseInt(timpSimulareTf.getText());
	}
	
	public JTextArea getEvolutieClient() {
		return EvolutieClient;
	}
	
	public JTextArea getInformationClient() {
		return infoClient;
	}
	
	public int getNumarCase() {
		return Integer.parseInt(numarCaseTf.getText());
	}
	
	public int getNumarClientiPerCasa() {
		return Integer.parseInt(numarClientiPerCasaTf.getText());
	}
	
	public int getNumarClienti() {
		return Integer.parseInt(numarClientiTf.getText());
	}
	
	public JButton getStartBtn() {
		return startBtn;
	}
	

	
	public void setContorTimp(int value) {
		contorTimpLbl.setText(value + "");
	}
	
}
